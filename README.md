# futureOfJava

git clone https://gitlab.com/Tiesvandeven/futureofjava.git
git clone https://gitlab.com/jdriven/jslider/revealjs-template.git

//Make a symbolic link from revealjs-template\public\slides to C:\talks\futureofjava\slides, in windows this can be done with:
`mklink /J C:\talks\revealjs-template\public\slides C:\talks\futureofjava\slides`

Go to the project revealjs-template\public

start a webserver (with npm you can do `npx serve`)

or commit and push and go to this url: https://tiesvandeven.gitlab.io/futureofjava