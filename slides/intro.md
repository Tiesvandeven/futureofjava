# Modern java

<img style="float: left; height: 500px;" src="slides/images/jeroen.jpg">
<img style="float: right; height: 500px;" src="slides/images/ties.jpg">

---slide---

# Agenda
* Java 11 - 17
* 18 and beyond

---slide---

# Java 11 - 17

* Text blocks
* Helpful NullPointerExceptions
* Switch Expressions
* Records
* Sealed Classes
* Pattern Matching for instanceof
