## Records
<pre>
<code class="hljs java" style="max-height: 100%;font-size:200%">
record Point(int x, int y)

</code>
</pre>

---slide---

## Inline records

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">record Tuple(String name, int lenght){}

Stream.of("a")
  .map(it -> new Tuple(it, it.length()))
  .collect(Collectors.toList());
	
</code>
</pre>

---slide---

## Polymorphism
<s>
<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">record Point(int x, int y) extends Shape

</code>
</pre>
</s>

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">record Point(int x, int y) implements Shape

</code>
</pre>

---slide---

## Data vs logic

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">record Data(String mydata){}


class SideEffects {
 
  public void store(Data data) { ... }
 
}

</code>
</pre>

---slide---

## Sealed Classes/Interfaces

* Restrict extension
* Stateful enums

---slide---

## Sealed Classes/Interfaces

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">public sealed class Shape permits Circle, Square, Rectangle {
}

</code>
</pre>

---slide---

## Pattern Matching for instanceof - old

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">if (obj instanceof String) {
    String s = (String) obj;
}

</code>
</pre>

---slide---

## Pattern Matching for instanceof - new

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">if (obj instanceof String s) {
    // can use s here
	System.out.println(s.lenght());
} else {
    // can't use s here
}

</code>
</pre>