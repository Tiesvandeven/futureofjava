## Project Loom
improving concurrency
* easy to use
* high throughput
* lightweight concurrency
* new programming model

---slide---

### virtual threads (1/2)

```java
Thread thread = Thread.ofVirtual().start(() -> System.out.println("Hello"));
thread.join();
```

* managed by JVM
* cheap to create 
* enables thread-per-request style
* do not pool

---slide---

### virtual threads (2/2)
```java
try (var executor = Executors.newVirtualThreadPerTaskExecutor()) {
    IntStream.range(0, 10_000).forEach(i -> {
        executor.submit(() -> {
            Thread.sleep(Duration.ofSeconds(1));
            return i;
        });
    });
} 
```

---slide---

### structured concurrency API (1/3)

complex concurrency made simple

splitting tasks into concurrent subtasks and joining them

---slide---

### structured concurrency API (2/3)
```java
Response handle() throws IOException {
    String user  = findUser();
    int    order = fetchOrder();
    return new Response(user, order);
}
```

---slide---

### structured concurrency API (3/3)
```java
Response handle() throws ExecutionException, InterruptedException {
    try (var scope = new StructuredTaskScope.ShutdownOnFailure()) {
        Future&lt;String>  user  = scope.fork(() -> findUser()); 
        Future&lt;Integer> order = scope.fork(() -> fetchOrder());

        scope.join();
        scope.throwIfFailed();

        return new Response(user.resultNow(), order.resultNow());
    }
}
```

