## Project Panama
Improving JVM <-> Native

Several APIs currently incubating
* Vector API
* Foreign-Memory Access API
* Foreign Linker API


---slide---

### Vector API (1/3)

* enable direct use of modern CPU instructions to handle multiple calculations (SIMD)
* platform independent
* graceful degradation

---slide---

### Vector API (2/3)

calculate c = - (a<sup>2</sup> + b<sup>2</sup>)
```java
void scalarComputation(float[] a, float[] b, float[] c) {
   for (int i = 0; i < a.length; i++) {
        c[i] = (a[i] * a[i] + b[i] * b[i]) * -1.0f;
   }
}
```

---slide---

### Vector API (3/3)
calculate c = - (a<sup>2</sup> + b<sup>2</sup>)

```java
static final VectorSpecies&lt;Float> SPECIES = FloatVector.SPECIES_PREFERRED;

void vectorComputation(float[] a, float[] b, float[] c) {
    for (int i = 0; i < a.length; i += SPECIES.length()) {
        var m = SPECIES.indexInRange(i, a.length);
		// FloatVector va, vb, vc;
        var va = FloatVector.fromArray(SPECIES, a, i, m);
        var vb = FloatVector.fromArray(SPECIES, b, i, m);
        var vc = va.mul(va).
                    add(vb.mul(vb)).
                    neg();
        vc.intoArray(c, i, m);
    }
}
```

---slide---

### Foreign Memory Access API (1/2)
Current ways to access off heap memory
* ByteBuffer (limited)
* sun.misc.Unsafe (unsafe :-) )
* jni (complex)

safe and easy to use alternative to `sun.misc.Unsafe`

---slide---

### Foreign Memory Access API (1/2)
```java
try (MemorySegment segment = MemorySegment.allocateNative(100)) {
   ...
}
```
---slide---

### Foreign Linker API (1/2)
* easy to use interoperability with C libraries
* 'pure Java'
* alternative to JNI

---slide---

### Foreign Linker API (2/2)

```java
MethodHandle strlen = CLinker.getInstance().downcallHandle(
        LibraryLookup.ofDefault().lookup("strlen"),
        MethodType.methodType(long.class, MemoryAddress.class),
        FunctionDescriptor.of(C_LONG, C_POINTER)
    );

try (MemorySegment str = CLinker.toCString("Hello")) {
long len = strlen.invokeExact(str.address());

```
