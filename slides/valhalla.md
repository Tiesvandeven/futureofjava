## Project Valhalla

> "Project Valhalla plans to augment the Java object model with value objects and user-defined primitives, combining the abstractions of object-oriented programming with the performance characteristics of simple primitives."

* Value Objects
* Primitive Classes
* Classes for the Basic Primitives
* Universal Generics

---slide---

### Value Objects (1/2)
New keywords: `value` and `identity`

Value classes:
* are implicitly final
* their instance fields are implicitly final

Value objects:
* do not have identity
* cannot mutate their fields
* cannot be used for synchonization

---slide---

### Value Objects (2/2)

== operator for value objects compares their fields, not their identiy!

you can have value record clases, value abstract classes and value interfaces

```java
value record Point(int x, int y)

Point a = new Point(3,2);
Point b = new Point(3,2);
assert a == b;

```


---slide---

### Primitive classes

Similar to value classes, except:

* Like primitives do now allow null values 
* Have default value

---slide---

### Classes for the Basic Primitives
Wrapper classes become primitive classes
* `java.lang.Byte`
* `java.lang.Short`
* `java.lang.Integer`
* `java.lang.Long`
* `java.lang.Float`
* `java.lang.Double`
* `java.lang.Boolean`
* `java.lang.Character`

Primitives are treated instances of these classes 

---slide---

### Universal Generics
Allow value classes and primitive classes as type in Generics

`List<int> ints = new ArrayList<>()`