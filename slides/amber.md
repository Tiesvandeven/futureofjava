## Future of Java

* Project Amber
* Project Loom
* Project Valhalla
* Project Panama

---slide---

## Project Amber
Small productivity-oriented language features.

---slide---

## Pattern Matching for switch

---slide---

## Remember this one

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">if (obj instanceof String s) {
    // can use s here
	System.out.println(s.lenght());
} else {
    // can't use s here
}

</code>
</pre>

---slide---

### Match on instance

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">Object o = 123L;
String formatted = switch (o) {
  case Integer i -> String.format("int %d", i);
  case Long l    -> String.format("long %d", l);
  case Double d  -> String.format("double %f", d);
  case String s  -> String.format("String %s", s);
  default        -> o.toString();
};

</code>
</pre>


---slide---

### Guarded pattern

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%" data-line-numbers="|4|6|8">Integer i = ...
switch (i) {

  case -1, 1 				-> ...   // Special cases
  
  case Integer i when i > 0  -> ...   // Positive integer cases
  
  case Integer i 			-> ...   // All the remaining integers
}

</code>
</pre>

* Syntax is not final yet

---slide---

### Exhaustive matching with sealed classes

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">static String switchStatementExhaustive(Shape s) {
  return switch (s) {
  
    case Circle c		-> "i'm a circle";
	
    case Rectangle r	 -> "i'm a rectangle";
	
    case Square s		-> "i'm a square";
  };
} 

</code>
</pre>

* No default needed (or advised)

---slide---

### Data driven programming

---slide---

### Error handling

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">public static String getResult(int i){
  if(i > 10) {
    return "greater that 10";
  }
  throw new IllegalStateException("smaller than 10");
}

</code>
</pre>

---slide---

### Error handling with sealed classes

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">public sealed interface Result{}

record Success(String data) implements Result{}

record Failure(Throwable error) implements Result{}

</code>
</pre>

---slide---

### Error handling with sealed classes

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">public static Result getResult(int i){
  if(i > 10) {
    return new Success("greater that 10");
  }
  return new Failure(new IllegalStateException("smaller than 10"));
}

</code>
</pre>

---slide---

## Error handling with sealed classes

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%" data-line-numbers="1-10|11-19|1-19">//Turn result into a String
public static String handleResult() {
  return switch (getResult(1)){
  
    case Success s -> s.data();
	
    case Failure f -> f.error().getMessage();
  };
}

//Or into a boolean
public static boolean handleResult() {
  return switch (getResult(1)){
  
    case Success s -> true;
	
    case Failure f -> false;
  };
}

</code>
</pre>

---slide---

## Record Patterns

---slide---

### deconstructing

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">record Point(int x, int y){}

static void printSum(Object o) {

  if (o instanceof Point point) {
  
    int x = point.x();
    int y = point.y();
    System.out.println(x+y);
  }
}

</code>
</pre>

---slide---

### deconstructing

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">record Point(int x, int y){}

void printSum(Object o) {

  if (o instanceof Point(int x, int y)) {
  
    System.out.println(x+y);
  }
}

</code>
</pre>

---slide---

### Record pattern in switch
<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%" data-line-numbers="1-9|3|5|7">String value = switch (p2) {

  case Point(var x, _) with x > 10	-> "x > 10";
  
  case Point(_, var y) with y > 10 	-> "x < 10 && y > 10";
  
  case Point(var x, var y)			-> "x < 10 && y < 10";
}

</code>
</pre>

* Must be exhaustive

---slide---

## String templating

(Still in design phase)

---slide---

## Current situation

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">String value = "world";

System.out.println("Hello " + value);

System.out.println(String.format("Hello %s", value));

</code>
</pre>

---slide---

### String interpolation

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">var name = "Bob";

var age = 10;

var s = STR."Hello, \{name}, I am \{age} years old";

//"Hello, Bob, I am 10 years old"

</code>
</pre>


---slide---

### String templating

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">interface TemplatePolicy&lt;T> {

    T apply(String templateString, List&lt;Object> parameters);
	
}

</code>
</pre>

---slide---

### String templating

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%" data-line-numbers="1-4|4-16">SimplePolicy&lt;JSONObject> JSON = 

  (TemplatedString ts) -> new JSONObject(ts.concat());

String name = "Joan Smith";

JSONObject doc = JSON."""

    {
	
        "name":    "\{name}"
		
    };
	
    """;

</code>
</pre>


---slide---

### Validation and normalization

<pre>
<code class="hljs java" style="max-height: 100%;font-size:150%">TemplatePolicy&lt;JSONObject, JSONException> 

  JSON_VERIFY = (TemplatedString ts) -> {
	
	

    String stripped = ts.concat().strip();
	
    if (!stripped.startsWith("{") || !stripped.endsWith("}")) {
        throws new JSONException("Missing brace");
    }
	
    return new JSONObject(stripped);
};

</code>
</pre>