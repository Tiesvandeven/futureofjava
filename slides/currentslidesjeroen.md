## Text Blocks

```java
ScriptEngine engine = new ScriptEngineManager().getEngineByName("js"); 
Object obj = engine.eval(
"function hello() {\n" + 
"	print('\"Hello, world\"');\n" + 
"}\n" + 
"\n" + 
"hello();\n"); 
```
```java
ScriptEngine engine = new ScriptEngineManager().getEngineByName("js"); 
Object obj = engine.eval(""" 
function hello() { 
	print('"Hello, world"'); 
} 
hello(); 
"""); 
```

---slide---


## Helpful NullPointerExceptions (1/2)

```java
String a = "abc";
String b = null;

int totalLength =  a.length() + b.length();
```

<pre>
Exception in thread "main" java.lang.NullPointerException:
 Cannot invoke "String.length()" because "b" is null at
 NPExample.main(NPExample.java:8)
</pre>

---slide---

## Helpful NullPointerExceptions (2/2)

```java
System.out.println(wrapper.x.y.z);

System.out.println(wrapper.getX().getY().getZ());
```

<pre>
Exception in thread "main" java.lang.NullPointerException:
 Cannot read field "z" because "wrapper.x.y" is null
 at NPExample.main(NPExample.java:37)

Exception in thread "main" java.lang.NullPointerException:
 Cannot invoke "NPExample$WrapperY.getY()" because the return
 value of "NPExample$WrapperX.getX()" is null
 at NPExample.main(NPExample.java:39)
</pre>

---slide---

## Switch Expressions (1/2)

<pre><code class="hljs java" style="max-height: 100%;float: left">
switch (day) {
    case MONDAY:
    case FRIDAY:
    case SUNDAY:
        System.out.println(6);
        break;
    case TUESDAY:
        System.out.println(7);
        break;
    case THURSDAY:
    case SATURDAY:
        System.out.println(8);
        break;
    case WEDNESDAY:
        System.out.println(9);
        break;
}
</code></pre>

<pre><code class="hljs java" style="max-height: 100%;float: right">
switch (day) {
    case MONDAY, FRIDAY, SUNDAY -> System.out.println(6);
    case TUESDAY                -> System.out.println(7);
    case THURSDAY, SATURDAY     -> System.out.println(8);
    case WEDNESDAY              -> System.out.println(9);
}
</code></pre>

---slide---

## Switch Expressions (2/2)

<pre><code class="hljs java" style="max-height: 100%;float: left">
int numLetters;
switch (day) {
    case MONDAY:
    case FRIDAY:
    case SUNDAY:
        numLetters = 6;
        break;
    case TUESDAY:
        numLetters = 7;
        break;
    case THURSDAY:
    case SATURDAY:
        numLetters = 8;
        break;
    case WEDNESDAY:
        numLetters = 9;
        break;
    default:
        throw new IllegalStateException("Incorrect: " + day);
}
</code></pre>

<pre><code class="hljs java" style="max-height: 100%;float: right">
int numLetters = switch (day) {
    case MONDAY, FRIDAY, SUNDAY -> 6;
    case TUESDAY                -> 7;
    case THURSDAY, SATURDAY     -> 8;
    case WEDNESDAY              -> 9;
};
</code></pre>
